<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PagesController@indexPage')->name('index-page');

Route::get('register', 'UsersController@createUsers')->name('register');
Route::post('register', 'UsersController@storeUsers')->name('store-user');

Route::get('login', 'UsersController@loginUser')->name('login-page');
Route::post('login', 'UsersController@findUser')->name('find-user');

Route::get('logout', 'UsersController@logout')->name('log-out');

Route::group(['middleware' => 'auth'], function() {

Route::get('member/index', 'UboardController@index')->name('user-board');


Route::get('member/subscriber/location', 'SubscribersController@getSubscribers')->name('subs-location');
Route::post('member/subscriber/location', 'WeatherController@getWeather')->name('get-locations');



Route::get('member/weather' ,'WeatherController@getWeather')->name('subs-weather');


});
