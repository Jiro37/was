<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>WAS</title>
  <!-- Bootstrap core CSS -->
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Icon css link -->
    <link href="{{ asset('css/fa-svg-with-js.css')}}" rel="stylesheet">

    <!-- Custom styles -->
    <link href="{{ asset('css/sb-admin.css')}}" rel="stylesheet">
</head>
