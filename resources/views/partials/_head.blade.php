<head>
	<title>WAS @yield('title')</title>

	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Icon css link -->
    <link href="{{ asset('css/fa-svg-with-js.css')}}" rel="stylesheet">

    <!-- Custom styles -->
    <link href="{{ asset('css/was-style.css')}}" rel="stylesheet">

</head>