@extends('main')

@section('contents')
	 <!-- Masthead -->
    <header class="masthead text-white text-center">
    <div class="overlay"></div>
      <div class="container">
        <div class="row">
        	<div class="col-xl-9 mx-auto">
            <h1 class="mb-5">WAS is a standalone application that interacts with the Viamo Platform using our APIs</h1>
          </div>
          <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
            <div>
      <a href=" {{route('login-page')}} " class="btn btn-danger " type="button" >Sign In</a>
		
			<a href=" {{route('register')}} " type="button" class="btn btn-info ">Create an Account</a>
			
		</div>
          </div> 
        </div>
	</div>
</header>

        
<section class="service_feature">
    <div class="container">
        <div class="title text-center ">
          <h1 class="mb-10">Our Services</h1>
          <p>Weather Alerts System</p>
        </div>
        <div class="row feature_inner">
            <div class="col-lg-4 col-sm-6">
                <div class="feature_item">
                    <div class="w_icon">
                        <span class="fas  fa-home fa-spin"></span>
                    </div>
                    <h4>Availability</h4>
                    <p>Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio ve</p>
                    <a type="button" class="btn btn-info" href="#" data-toggle="modal" data-target="#exampleModal" >Read More</a>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="feature_item">
                    <div class="w_icon">
                        <span class="fas fa-tv fa-spin"></span>
                    </div>
                    <h4>Easy to Use</h4>
                    <p>Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio ve</p>
                    <a type="button" class="btn btn-info" href="#" data-toggle="modal" data-target="#exampleModal" >Read More</a>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="feature_item">
                    <div class="w_icon">
                        <span class="fas fa-cog fa-spin"></span>
                    </div>
                    <h4> Settings </h4>
                    <p>Etiam nec odio vestibulum est mattis effic iturut magna. Pellentesque sit am et tellus blandit. Etiam nec odio ve</p>
                    <a type="button" class="btn btn-info" href="#" data-toggle="modal" data-target="#exampleModal" >Read More</a>
                </div>
            </div>
                   
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
@endsection