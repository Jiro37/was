<!DOCTYPE html>
<html>
  @include('partials._head')
<body id="">

	 @include('partials._nav')

   @yield('contents')

  @include('partials._footer')

  @include('partials._javascripts')

</body>
</html>