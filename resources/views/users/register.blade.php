@extends('main')

@section('contents')

<body class="hold-transition register-page">
  <div class="register-box">
  <div class="register-logo">
    <a href="{{route('index-page')}}"><b>WAS</b></a>
  </div>

  <div class="card">
    <div class="card-body register-card-body">
      <p class="login-box-msg">Register a new membership</p>

      <form action="{{ route('store-user')}}" method="post">
        {{csrf_field()}}
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

        <div class="form-group">
          <input type="text" name="api_key" class="form-control" placeholder="Your Api Key">
          <span class=" form-control-feedback"></span>
        </div>
        <div class="form-group">
          <input type="text" name="name" class="form-control" placeholder="Full name">
          <span class=" form-control-feedback"></span>
        </div>
        <div class="form-group">
          <input type="email" name="email" class="form-control" placeholder="Email">
          <span class=" form-control-feedback"></span>
        </div>
        <div class="form-group">
          <input type="password" name="password" class="form-control" placeholder="Password">
          <span class=" form-control-feedback"></span>
        </div>
        <div class="form-group">
          <input type="password" name="password_confirmation" class="form-control" placeholder="Retype password">
          <span class=" form-control-feedback"></span>
        </div>
        <div class="form-group">
          <input type="text" name="phone_number" class="form-control" placeholder="Phone Number" min="0100000000" max="9999999999">
          <span class=" form-control-feedback"></span>
        </div>
        <div class="form-group">
          <input type="text" name="location" class="form-control" placeholder="Location">
          <span class=" form-control-feedback"></span>
        </div>
        <div class="form-group">
          <input type="text" name="language" class="form-control" placeholder="Language">
          <span class=" form-control-feedback"></span>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="checkbox icheck">
              <label>
                <input type="checkbox"> I agree to the <a href="#">terms</a>
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <a href="{{route('login-page')}}" class="text-center">I already have a membership</a>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

@endsection



