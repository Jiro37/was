<!DOCTYPE html>
<html lang="en">

@include('partials.b_head')

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  @include('partials.b_nav')

  <div class="content-wrapper">
    <div class="container-fluid">
        @yield('boardcontent')  
    </div>
  
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    @include('partials.b_footer')
    
    @include('partials.b_logout')

    @include('partials.b_javascripts')

  </div>
</body>

</html>
