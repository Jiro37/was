<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'api_key','name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


     public static function saveUsers($request){
      $newUser = new User;

      $newUser->api_key = $request['api_key'];
      $newUser->name = $request['name'];
      $newUser->email = $request['email'];
      $newUser->password = bcrypt($request['password']);
      $newUser->phone_number = $request['phone_number'];
      $newUser->location = $request['location'];
      $newUser->language = $request['language'];

      $newUser->save();
      return redirect()->route('register');

    }
}
