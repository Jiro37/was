<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

use GuzzleHttp\Exception\GuzzleException;

class SubscribersController extends Controller
{
    //
    public function getSubscribers()
    {

    	$api_key = Auth::user()->api_key;

    	//dd($api_key);
    	$client = new \GuzzleHttp\Client();
    	//$url = "https://go.votomobile.org/api/v1/subscribers";
    	//readfile($url .'?api_key='.$api_key);

		$response = $client->get('https://go.votomobile.org/api/v1/subscribers'.'?api_key='.$api_key, [
    		'headers' => [
        	'Authorization' => $api_key,
    		],
	    // 'form_params' => [
	    //     'name' => '',
	    //     'location' => '',
	        
	    // ],
		]);

		// You need to parse the response body
		// This will parse it into an array
		$response = json_decode($response->getBody(), true);
		//dd($response);


		
        
    	return view('uboard.index')->withResponse($response);
    }


		

    }
