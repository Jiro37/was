<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class UsersController extends Controller
{
    //
    public function createUsers()
    {
    	return view('users.register');
    }

    public function storeUsers(Request $request)
    {
    	$this->validate($request, [

    		'api_key' => 'required',
            'name' => 'required',
    		'email' => 'email|required|unique:users',
    		'password' => 'required|confirmed',
    		'phone_number' => 'required|unique:users',
    		'location' => 'required',
    		'language' => 'required',

    	]);

    	User::saveUsers($request);

    	


        return redirect()->route('subs-location');
    }

    public function loginUser() 
    {
    	return view('users.login');
    }

    public function findUser(Request $request)
    {
 		if (Auth::attempt([
 			'email' => $request['email'],
 			'password' => $request['password']
 		])) {
 			return redirect()->route('subs-location');
 		}
        else{
 			return "Wrong name or password";
 		}
    }

    public function logout (Request $request){
        Auth::logout();
        return redirect()->route('index-page');
    }
}
